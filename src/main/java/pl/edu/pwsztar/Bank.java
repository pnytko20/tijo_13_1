package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

// TODO: Prosze dokonczyc implementacje oraz testy jednostkowe
// TODO: Prosze nie zmieniac nazw metod - wszystkie inne chwyty dozwolone
// TODO: (prosze jedynie trzymac sie dokumentacji zawartej w interfejsie BankOperation)
class Bank implements BankOperation {

    private static int accountNumber = 0;

    List<Account> accounts = new ArrayList<Account>();

    public int createAccount() {
        accountNumber++;
        accounts.add(new Account(accountNumber, 0));
        return accountNumber;
    }

    public int deleteAccount(int accountNumber) {
        if (accountNumber < 0) {
            return ACCOUNT_NOT_EXISTS;
        }
        for (Account account : accounts) {
            if (account.getAccountNumber() == accountNumber) {
                accounts.remove(account);
                return account.getAccountBalance();
            }
        }
        return ACCOUNT_NOT_EXISTS;
    }

    public boolean deposit(int accountNumber, int amount) {
        for (Account account : accounts) {
            if (account.getAccountNumber() == accountNumber) {
                if (amount < 0) {
                    return false;
                } else if (amount > 0) {
                    account.setAccountBalance(account.getAccountBalance() + amount);
                    return true;
                }
            }
        }
        return false;
    }

    public boolean withdraw(int accountNumber, int amount) {
        for (Account account : accounts) {
            if (account.getAccountNumber() == accountNumber) {
                if (account.getAccountBalance() < amount) {
                    return false;
                }
                if (amount > 0) {
                    account.setAccountBalance(account.getAccountBalance() - amount);
                    return true;
                }
            }
        }
        return false;
    }

    public boolean transfer(int fromAccount, int toAccount, int amount) {
        boolean moneyDeposited = false;
        boolean moneyWithdrawn = false;
        for (Account account : accounts) {
            if (account.getAccountNumber() == fromAccount && toAccount != BankOperation.ACCOUNT_NOT_EXISTS && fromAccount != BankOperation.ACCOUNT_NOT_EXISTS) {
                if (account.getAccountBalance() >= amount && amount > 0) {
                    withdraw(fromAccount, amount);
                    moneyWithdrawn = true;
                } else {
                    return false;
                }
            } else if (account.getAccountNumber() == toAccount && toAccount != BankOperation.ACCOUNT_NOT_EXISTS && fromAccount != BankOperation.ACCOUNT_NOT_EXISTS) {
                deposit(toAccount, amount);
                moneyDeposited = true;
            }
        }
        return moneyDeposited && moneyWithdrawn;
    }

    public int accountBalance(int accountNumber) {
        for (Account account : accounts) {
            if (account.getAccountNumber() == accountNumber) {
                return account.getAccountBalance();
            }
        }
        return ACCOUNT_NOT_EXISTS;
    }

    public int sumAccountsBalance() {
        int sumOfAccounts = 0;
        for (Account account : accounts) {
            sumOfAccounts += account.getAccountBalance();
        }
        return sumOfAccounts;
    }
}
